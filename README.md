# Currency Converter Front-End

> A simple one-direction currency converter for the CZK currency.

## Deployment

Expected to be deployed at [momence-cc-fe.vercel.app](https://momence-cc-fe.vercel.app/).

## Notes

- Doesn't call the CNB API directly due to CORS limitations, but rather uses the back-end API
  (expected to be deployed at [momence-cc-be.vercel.app](https://momence-cc-be.vercel.app/)). 
- Arguably, using [Vite](https://vitejs.dev/) is now preferred over [Create React App](https://create-react-app.dev/)
  as CRA is no longer actively developed.
  The new React docs even don't recommend using React without a framework like Next.js
  (see [here](https://react.dev/learn/start-a-new-react-project)).
- React Query considers the data stale and re-fetches them [quite often](https://tanstack.com/query/v4/docs/react/guides/important-defaults)
  (e.g., on a window re-focus) - that can be adjusted to one's liking.

## Known Issues and Limitations

- Model for the currency exchange rates is the same as the one used in the back-end; it should be moved to a shared library,
  generated from OpenAPI spec or similar.
- I opted to use [React Bootstrap](https://react-bootstrap.netlify.app/) to quick build a nice and easy UI.
  I ended up not needing [Styled Components](https://styled-components.com/) (as mentioned in the requirements),
  but I could and would of course use those if I were to customize the UI more.
- It would be nice to add a switch to change the conversion direction (e.g. from CZK to EUR or vice versa).
