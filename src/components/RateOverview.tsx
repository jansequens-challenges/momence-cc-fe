import React from 'react';
import {rateRecordSortComparator} from '../util/utils.ts';
import {Table} from 'react-bootstrap';
import {CnbRateRecord} from '../model/cnbRateRecord.model.ts';

export interface RateTableProps {
    rates: CnbRateRecord[] | undefined;
    selected?: CnbRateRecord;
}

export const RateOverview: React.FC<RateTableProps> = ({rates, selected}) => {

    if (!rates || rates.length == 0) return null;

    return (
        <Table striped bordered hover size="sm">
            <thead>
                <tr>
                    <th>Country</th>
                    <th>Currency</th>
                    <th>Currency Name</th>
                    <th>Amount</th>
                    <th>Rate</th>
                </tr>
            </thead>
            <tbody>
                {rates.sort(rateRecordSortComparator).map((record) => (
                    <tr key={record.currencyCode} className={record.currencyCode === selected?.currencyCode ? 'table-warning' : ''}>
                        <td>{record.country}</td>
                        <td>{record.currencyCode}</td>
                        <td>{record.currencyName}</td>
                        <td>{record.amount} <span className="text-muted">{record.currencyCode}</span></td>
                        <td>{record.rate} <span className="text-muted">CZK</span></td>
                    </tr>
                ))}
            </tbody>
        </Table>
    );
}
