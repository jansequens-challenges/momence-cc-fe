import {CnbRates} from '../model/cnbRates.model.ts';
import React, {useState} from 'react';
import Form from 'react-bootstrap/Form';
import {Col, InputGroup, Row} from 'react-bootstrap';
import {rateRecordSortComparator} from '../util/utils.ts';
import {CnbRateRecord} from '../model/cnbRateRecord.model.ts';

export interface RateCalculatorProps {
    rates: CnbRates | undefined;
    selected: CnbRateRecord | undefined;
    onCurrencyChange?: (currency: CnbRateRecord) => void;
    roundToDecimals?: number;
}

export const RateCalculator: React.FC<RateCalculatorProps> = ({rates, selected, onCurrencyChange, roundToDecimals = 2}) => {

    const effectiveRecords: CnbRateRecord[] = (rates?.records ?? []).sort(rateRecordSortComparator);
    const roundToDecimalsInt = (roundToDecimals < 0 || roundToDecimals >= 11) ? Math.floor(roundToDecimals) : 2;

    // anything that can be interpreted as a number with up to 2 decimal places (both comma and dot are accepted as decimal separator)
    const validAmountStringRegexp = new RegExp('^\\d*(?:[.,]\\d{0,2}$)?$');

    const [amountString, setAmountString] = useState<string>("100");

    if (!rates || rates.records.length == 0) return null;

    // event handlers

    const handleAmountStringChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const newAmount = event.target.value;
        validAmountStringRegexp.test(newAmount) && setAmountString(newAmount);
    }

    const handleCurrencyChange = (event: React.ChangeEvent<HTMLSelectElement>): void => {
        const currencyCode = event.target.value;
        const record = effectiveRecords.find((record) => record.currencyCode === currencyCode);
        if (record && onCurrencyChange) {
            onCurrencyChange(record);
        }
    }

    // business logic

    const toNumber = (amountString: string): number | undefined => {
        const amountNormalizedSeparator = amountString.replace(',', '.');
        if (amountNormalizedSeparator === '' || amountNormalizedSeparator === '.') return 0;
        Number.parseFloat(amountNormalizedSeparator);
        try {
            return Number.parseFloat(amountNormalizedSeparator)
        } catch (e) {
            // should not happen as the amount string is validated by a regexp
            return undefined;
        }
    }

    const convert = (amountToConvert: string, targetCurrency?: CnbRateRecord): string => {
        if (!targetCurrency) return "?";
        const amountNormalizedSeparator = amountToConvert.replace(',', '.');
        if (amountNormalizedSeparator === '' || amountNormalizedSeparator === '.') return Number(0).toFixed(roundToDecimalsInt);
        const amount = toNumber(amountNormalizedSeparator);
        if (!amount) return "?";
        const resultRaw = (amount / targetCurrency.rate) * targetCurrency.amount;
        const roundFactor = Math.pow(10, roundToDecimalsInt);
        return (Math.round(resultRaw * roundFactor) / roundFactor).toFixed(roundToDecimalsInt);
    }

    // render

    return <Form>
        <Row>
            <Col>
                <InputGroup>
                    <Form.Control placeholder="Enter amount in CZK" value={amountString} onChange={handleAmountStringChange} />
                    <InputGroup.Text>CZK</InputGroup.Text>
                </InputGroup>
                <Form.Text muted>Enter amount in CZK to convert</Form.Text>
            </Col>
            <Col>
                <Form.Select
                    aria-label="Select currency"
                    value={selected?.currencyCode}
                    onChange={handleCurrencyChange}
                >
                    {rates.records.sort(rateRecordSortComparator).map((record) => (
                        <option
                            value={record.currencyCode}
                            key={record.currencyCode}
                        >
                            {record.country} ({record.currencyCode})
                        </option>
                    ))}
                </Form.Select>
                <Form.Text muted>Select the target currency</Form.Text>
            </Col>
            <Col>
                <div className="ms-2">
                    <div className="text-muted">{toNumber(amountString) ?? 0} CZK =</div>
                    <div className="fs-3">
                        <span className="align-self-center">{convert(amountString, selected)}</span>
                        <span className="align-self-center text-muted d-inline-block ms-2">{selected?.currencyCode}</span>
                    </div>
                </div>
            </Col>
        </Row>

    </Form>
}
