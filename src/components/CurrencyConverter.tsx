import {useQuery} from '@tanstack/react-query';
import axios from 'axios';
import {CnbRates} from '../model/cnbRates.model.ts';
import {CnbApiError} from '../model/cnbApiError.model.ts';
import {RateOverview} from './RateOverview.tsx';
import {RateCalculator} from './RateCalculator.tsx';
import {Col, Container, Row} from 'react-bootstrap';
import React, {useEffect, useState} from 'react';
import {CnbRateRecord} from '../model/cnbRateRecord.model.ts';

export const CurrencyConverter: React.FC = () => {

    const ratesUrl = 'https://momence-cc-be.vercel.app/api/rates'
    const defaultCurrencyCode = 'USD';

    const [selectedCurrency, setSelectedCurrency] = useState<CnbRateRecord | undefined>(undefined)

    const {isPending, error, data, isFetching} = useQuery<CnbRates, CnbApiError>({
        queryKey: ['rates'], queryFn: () =>
            axios
                .get<CnbRates>(ratesUrl)
                .then((res) => res.data),
    })

    // effects

    // sets the selected currency to default (if needed)
    useEffect(() => {
        if (data && data.records.length > 0 && (!selectedCurrency || !data.records.includes(selectedCurrency))) {
            // data are not empty and the selected currency is either not set or no longer present in the data
            const usdCurrency = data.records.find((record) => record.currencyCode === defaultCurrencyCode);
            const defaultCurrency = usdCurrency ?? data.records[0];
            setSelectedCurrency(defaultCurrency)
        }
    }, [data, selectedCurrency]);

    // event handlers

    const handleCurrencyChange = (currency: CnbRateRecord): void => {
        setSelectedCurrency(currency)
    }

    // render

    if (error) return 'Could not load exchange rates: ' + error.message

    return (
        <Container className="mt-4 mb-4">
            <Row className="mb-4">
                <Col>
                    <h1>Currency Converter</h1>
                </Col>
            </Row>
            <Row className="mb-4">
                <Col>
                    {
                        (isPending || isFetching)
                            ? 'Loading rates from CNB...'
                            : `Czech National Bank exchange rates as of ${new Date(data.date).toLocaleDateString()}`
                    }
                </Col>
            </Row>
            <Row className="mb-5">
                <Col><RateCalculator rates={data} selected={selectedCurrency} onCurrencyChange={handleCurrencyChange} /></Col>
            </Row>
            <Row>
                <Col><RateOverview rates={data?.records} selected={selectedCurrency} /></Col>
            </Row>
        </Container>
    )
}
