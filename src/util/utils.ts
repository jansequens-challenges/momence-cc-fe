import {CnbRateRecord} from '../model/cnbRateRecord.model.ts';

export const rateRecordSortComparator = (a: CnbRateRecord, b: CnbRateRecord) => a.country.localeCompare(b.country);
