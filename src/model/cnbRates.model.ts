import {CnbRateRecord} from './cnbRateRecord.model';

export interface CnbRates {

    // ISO 8601 date format (YYYY-MM-DD)
    date: string;

    records: CnbRateRecord[];
}
