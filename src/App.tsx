import {QueryClient, QueryClientProvider,} from '@tanstack/react-query'
import {CurrencyConverter} from './components/CurrencyConverter.tsx';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {
    const queryClient = new QueryClient({
        defaultOptions: { queries: { retry: false } },
    })

    return (
        <QueryClientProvider client={queryClient}>
            <CurrencyConverter />
        </QueryClientProvider>
    )
}

export default App
